class GildedRose

	@@max_quality = 50 # Максимальное качество для обычного товара
	@@reduce_quality_2x = 10 # Граница качества при котором его снижение увеличивается в 2 раза
	@@reduce_quality_3x = 5 # Граница качества при котором его снижение увеличивается в 3 раза

	# Названия особенностей товаров хранятся в виде имени константы. Возможно расширение списков товаров с особенностями.
	# Изначально условия задачи были поняты так, что «Aged Brie» помимо своего имеет функционал «Backstage passes».
	# Поэтому решил сделать возможным одному товару назначать разный функционал. После прочтения ТЗ на англ. понял ошибку, но решил оставить код в таком виде, т.к. может пригодится для дальнейшей разработки. 

	module SpecialItems
		LEGENDARY = ["Sulfuras"] # Нет срока хранения и не подвержен ухудшению качества
		EXTRA_QUALITY = ["Aged Brie"] # Качество увеличивается пропорционально возрасту
		SPECIAL_ABILITY = ["Backstage passes"] # Качество увеличивается по мере приближения к концу срока хранения
		DOUBLE_AGING = ["Conjured"] # Теряют качество в два раза быстрее
	end

	def initialize(items)
		@items = items

		# "Качество товара никогда не может быть отрицательным"
		# "Качество товара никогда не может быть больше, чем 50"
		# Считаю ошибкой возможность конструктора Item'a создать экземплял с данными вне диапозона, указанного в ТЗ

		@items.each do |item|
			limitationsHandler item
		end 
	end

	# Возвращает массив названий особенностей товара
	def getSpecialAbilities item
		specialAbilities = []
		SpecialItems.constants.each do |list|
			SpecialItems.const_get(list).each do |special|
				# puts item.name.inspect
				# puts special.inspect
				# puts "---"
				if (!item.name.nil? && item.name.match(special) )
					specialAbilities.push list
				end
			end
		end
		return specialAbilities
	end

	def limitationsHandler item
		item.quality = 0 if item.quality < 0
		item.quality = @@max_quality if item.quality > @@max_quality
	end

	# Обрабатывает товар исходя из его особенности
	def itemAbilityHandler item, currentSellIn, ability = :REGULAR
		case ability
			when :REGULAR
				item.sell_in = currentSellIn - 1
				item.quality -= 1
				item.quality -= 1 if item.sell_in <= 0
			when :EXTRA_QUALITY
				item.sell_in = currentSellIn - 1
				item.quality += 1
			when :LEGENDARY
				# do_nothing
			when :SPECIAL_ABILITY
				item.sell_in = currentSellIn - 1
				item.quality += 1
				item.quality += 1 if item.sell_in < @@reduce_quality_2x
				item.quality += 1 if item.sell_in < @@reduce_quality_3x
				item.quality = 0 if item.sell_in < 0
			when :DOUBLE_AGING
				item.sell_in = currentSellIn - 1
				item.quality -= 2
				item.quality -= 2 if item.sell_in <= 0
		end

		limitationsHandler item
	end

	def update_quality()
		@items.each do |item|
			specialAbilities = getSpecialAbilities item
			currentSellIn = item.sell_in

			if specialAbilities.any?
				# Т.к. у товара может быть несколько особых свойств, то каждую особенность через обработчик
				specialAbilities.each do |ability|
					itemAbilityHandler item, currentSellIn, ability
				end
			else
				#Поведение по умолчанию
				itemAbilityHandler item, currentSellIn
			end
		end
	end
end

class Item
	attr_accessor :name, :sell_in, :quality

	def initialize(name, sell_in, quality)
		@name = name
		@sell_in = sell_in
		@quality = quality
	end

	def to_s()
		"#{@name}, #{@sell_in}, #{@quality}"
	end
end