require File.join(File.dirname(__FILE__), 'gilded_rose')

describe GildedRose do

	describe "#update_quality" do
		RANDOM_NAME = ('a'..'z').to_a.shuffle[0,20].join
		BACKSTAGE_NAME = "Backstage passes to a TAFKAL80ETC concert"
		AGED_BRIE_NAME = "Aged Brie"
		SULFURAS_NAME = "Sulfuras, Hand of Ragnaros"
		CONJURED_NAME = "Conjured"

		# it "Aged Brie test" do
		# 	items = [Item.new(AGED_BRIE_NAME, 0, 0)]
		# 	GildedRose.new(items).update_quality()
		# 	expect(items[0].name).to have "Aged Brie"
		# end

		it "Random item sell_in and quality presence test" do
			items = [Item.new(@randomItemName, 1, 1)]
			items[0].sell_in.should_not be_nil
			items[0].quality.should_not be_nil
		end

		it "Random item sell_in and quality reduce test" do
			items = [Item.new(@randomItemName, 1, 1)]
			GildedRose.new(items).update_quality()
			expect(items[0].sell_in).to eq 0
			expect(items[0].quality).to eq 0
		end

		it "Random item double quality reduce test" do
			items = [Item.new(@randomItemName, 0, 2)]
			GildedRose.new(items).update_quality()
			expect(items[0].sell_in).to eq -1
			expect(items[0].quality).to eq 0
		end

		it "Random item negative quality construction test" do
			# Качество товара никогда не может быть отрицательным;
			# Данный тест невозможно пройти, т.к. по условию ТЗ нельзя менять конструктор Item'a
			# Item.new(@randomItemName, 0, -1).should raise_error()
			items = [Item.new(@randomItemName, 0, -1)]
			GildedRose.new(items)
			items[0].sell_in.should be >= 0
		end

		it "Random item negative quality update test" do
			items = [Item.new(@randomItemName, 2, 0)]
			GildedRose.new(items).update_quality()
			items[0].quality.should >= 0
		end

		it "Random item upper limit of quality construction test" do
			# Качество товара никогда не может быть больше, чем 50;
			# Данный тест невозможно пройти, т.к. по условию ТЗ нельзя менять конструктор Item'a
			# Item.new(@randomItemName, 0, 51).should raise_error()
			items = [Item.new(@randomItemName, 0, 100)]
			GildedRose.new(items)
			items[0].sell_in.should be <= 50
		end

		it "Backstage passes upper limit of quality increment test" do
			items = [Item.new(BACKSTAGE_NAME, 1, 50)]
			GildedRose.new(items).update_quality()
			items[0].quality.should <= 50
		end

		it "Aged Brie upper limit of quality increment test" do
			items = [Item.new(AGED_BRIE_NAME, 1, 50)]
			GildedRose.new(items).update_quality()
			items[0].quality.should <= 50
		end

		it "Sulfuras legendary test" do
			items = [Item.new(SULFURAS_NAME, -1, 10)]
			GildedRose.new(items).update_quality()
			expect(items[0].sell_in).to eq -1
			expect(items[0].quality).to eq 10
		end

		it "Backstage passes 10 to 5 sell_in reduce test" do
			items = [Item.new(BACKSTAGE_NAME, 10, 0), Item.new(AGED_BRIE_NAME, 10, 0)]
			gildedRose = GildedRose.new(items)
			5.times do
				gildedRose.update_quality()
			end
			expect(items[0].quality).to eq 5 * 2
			# Была ошибка в тесте из-за неверно понятого ТЗ на русском языке.
			# expect(items[1].quality).to eq 5 * 2
		end

		it "Backstage passes 5 to 0 sell_in reduce test" do
			items = [Item.new(BACKSTAGE_NAME, 5, 0), Item.new(AGED_BRIE_NAME, 5, 0)]
			gildedRose = GildedRose.new(items)
			5.times do
				gildedRose.update_quality()
			end
			expect(items[0].quality).to eq 5 * 3

			# Была ошибка в тесте из-за неверно понятого ТЗ на русском языке.
			# expect(items[1].quality).to eq 5 * 3
		end

		it "Backstage passes zero quality at concert day test" do
			items = [Item.new(BACKSTAGE_NAME, 0, 10), Item.new(AGED_BRIE_NAME, 0, 10)]
			GildedRose.new(items).update_quality()
			expect(items[0].quality).to eq 0

			# Была ошибка в тесте из-за неверно понятого ТЗ на русском языке.
			# expect(items[1].quality).to eq 0
		end

		# Тесты для нового функционала

		it "Conjured double qualityt reduce test" do
			items = [Item.new(CONJURED_NAME, 10, 2)]
			GildedRose.new(items).update_quality()
			expect(items[0].quality).to eq 0
		end

	end
end